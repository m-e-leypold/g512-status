# G512-Status -- Indicate capslock and numlock status by switching key colors
#                on a G512 keyboard (Logitech).
#
# Copyright (C) 2022 M E Leypold
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

g numeric fffff0
k numlock 803000
k num0 ff6000
k num1 00A0ff
k num2 ffff00
k num3 ffff00
k num4 ffff00
k num5 00A0ff
k num6 ffff00
k num7 00A0ff
k num8 ffff00
k num9 ffff00
k num. ff0000
k numenter 00A0ff
c # Commit changes
