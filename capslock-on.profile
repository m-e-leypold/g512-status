# G512-Status -- Indicate capslock and numlock status by switching key colors
#                on a G512 keyboard (Logitech).
#
# Copyright (C) 2022 M E Leypold
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

g modifiers 000000
g keys ff9020
k tab ff6000
k backspace ff0000
k enter 00A0ff

k capslock 00A0ff
k shiftl ff0000
k shiftr ff0000
c # Commit changes
