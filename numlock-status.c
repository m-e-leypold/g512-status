/*
 * G512-Status -- Indicate capslock and numlock status by switching key colors
 *                on a G512 keyboard (Logitech).
 * 
 * Copyright (C) 2022 M E Leypold
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/ 

#include <stdio.h>
#include <unistd.h>
#include <X11/Xlib.h>
#define MASK 0x2

int main(void) {
  usleep(100000);

  Display *display = XOpenDisplay(":1.0"); 
  XKeyboardState control_structure;
  XGetKeyboardControl(display, &control_structure);
  XCloseDisplay(display);
  
  printf("%s\n",(control_structure.led_mask & MASK) ? "on" : "off");
  return 0;
}
