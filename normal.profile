# G512-Status -- Indicate capslock and numlock status by switching key colors
#                on a G512 keyboard (Logitech).
#
# Copyright (C) 2022 M E Leypold
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

g logo 000096
g indicators 00ff00
g multimedia 009600
g fkeys 00A0ff
k f1 00ff00
k f2 ff6000
k f9 ff6000
k f10 ff6000
k f11 ff6000
k f12 ff6000

g modifiers ff6000
g arrows ffff00

g numeric fffff0
k numlock 00ff00
k numenter 00A0ff

g functions ffffff
k pagedown ffff00
k pageup ffff00
k ins ff6000
k del ff0000
k home 00A0ff
k end 00A0ff
k esc ff0000
k printscr 00A0ff
k scrolllock 00A0ff
k pausebreak 00A0ff

g keys ffffff
k tab ff6000
k backspace ff0000
k enter 00A0ff

g gkeys ffffff
k capslock 00A0ff
k gamemode ff4000
k capsindicator ff8000


c # Commit changes
