# G512-Status -- Indicate capslock and numlock status by switching key colors
#                on a G512 keyboard (Logitech).
#
# Copyright (C) 2022 M E Leypold
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


g logo 000096
g indicators ffffff
g multimedia 009600
g fkeys ff00ff
g modifiers ff0000
g arrows ffff00
g numeric 00ffff
g functions ffffff
g keys 009696
g gkeys ffffff

c # Commit changes
