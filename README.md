# g512-status

## About

This is a software package primarily to solve the problem that the
Logitech 512 and 513 keyboard do not have a numlock indicator light.

### History

I recently bought a Logitech g512 keyboard because I wanted a keyboard
with an aluminum plate and clicky switches and a numeric
keypad. Having already a TKL K835 for office use, I basically wanted
the same keyboard, but with a numeric keypad, since I'm using that for
control purposes habitually, like for example regulating the audio
volume of mplayer.

Turned out that there is no K835 variant with a numeric keypad and not
"normal" (non RGB) keyboard with a keypad an clicky (blue)
switches. There are so called tactile switches and Romer G switches
and I didn't trust any of them to fill my needs (I think for >100 EUR
per piece I'm entitled to get my needs satisfied 100%. It's different
for a 15 EUR keyboard, where the only requirement is: Typing must be
possible).

The G512 was the nearest to my requirements, but an RGB keyboard. So
the G513 is what I bought (actually under the label of G512).

Turned out: This keyboard doesn't have a numlock indicator light,
which I really find terribly bothersome. Only wailing in the
intertubes, no actual solution for that (Logitech: How could you? I
mean, WTF!).

So after a day or two I was thinking: Isn't here a way to switch the
color of the "NUM" key when numlock is toggled? That would be a good
substitute ...

I'm working under Linux privately, so I'd need a way to change the RGB
color with a program somehow. 

Turned out: There is a number of solutions for that and since I didn't
want too much smartness getting into my way, my decision was to use
the [g810-led package](https://github.com/MatMoul/g810-led) as the
foundation on which to build my scripts.

### What g512-status can do for you

- Possibility to configure the desktop to switch coloring of the numeric
  keypad according to whether numlock is on or off.
- Possibility to configure the desktop to switch coloring of the keyboard
  according to whether capslock is on or off --- This is very useful as a
  more visible indicator for a turned on capslock.
- A useful color profile for your RGB keyboard (inspired by g810-led).
- Some scripts to flash alarms via the keyboard (e.g. with 'g512-alarm
  2' and remove the alarm with 'g512-good' again). I haven't found any
  use for them yet, admittedly.

### Restrictions

- Manual installation procedure.
- No general color profiling support: You'll have to write them
  manually and understand what you're doing.
- Every account must have the scripts configured as explained above.
- Won't do anything for the console.
- There is a slight delay when updating the inidcators. This is due to
  the scripts waiting a 100 ms before reporting the indicator status
  from the display. This has been found empirically to be necessary
  and there is probably a race condition somewhere.

## How to build and install

- Go to the top level source directory and type

      $ make
	
  This will build the binaries that read the status of numlock and capslock from the display.
  
- The scripts and binaries (everything that is now executable) will have to
  be linked from or copied to your path.

- Copy all the profiles to ```/etc/g810-led/```.

- On XFCE:

  - Using *Settings → Keyboards → Application Shortcuts* configure
	```g512-update-indicators``` to be run when *Caps Lock* or *Num
	Lock* are pressed.

  - Using *Settings → Session and Startup → Application Autostart*
    configure ```g512-update-indicators``` to be run *on login*.
 
The last item item will have to be different for other desktop
environments, of course. I haven't tried it, though, and welcome
feedback and pull requests adding instructions for other desktops to
this README.

## License = GPL 3

*G512-status* is licensed unter the GNU GPL 3 as follows:

> G512-Status -- Indicate capslock and numlock status by switching key colors
>                on a G512 keyboard (Logitech).
>
> Copyright (C) 2022 M E Leypold
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.

The full license terms acn be found in file [LICENSE.md](file:LICENSE.md)

## Commandline scripts and tools

- ```numlock-status``` --- Outputs the numlock status on stdout.
- ```caps-status``` --- Outputs the numlock status on stdout.
- ```g512-update-indicators``` --- updates the color profile dependend on numlock and capslock status :
  - numlock on --- ```NUM``` is green, the numbers bluish white (like the alphabetic keys).
  - numlock off --- ```NUM``` is dark orange and the numeric keys are green and blue.
  - capslock off --- Modifier keys are orange, capslock key is blue, alphabetic and numeric keys are bluish white.
  - capslock off --- Modifier keys are dark, shift is red, alphabetic and number keys are orange-yellow.
- ```g512-restore``` --- restore normal color profile.
- ```g512-bad``` -- flash keyboard red for 3 seconds then restore.
- ```g512-alarm``` *n* -- For *n* in 1..4, start flashing keyboard red, the higher *n* the faster.
- ```g512-good``` -- flash keyboard green for 3 seconds, then restore normal color profile.

The scripts ```g512-bad```, ```g512-good```, ```g512-alarm``` provide
the means to visually signal alarms via the keyboard lights.

